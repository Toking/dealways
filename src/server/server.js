"use strict";

const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server, {serveClient: true});
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost:27017/dealways',{useNewUrlParser: true});
mongoose.Promise = require('bluebird');

server.listen(6666, '0.0.0.0', () =>{
    console.log('Server started on port 6666');
});