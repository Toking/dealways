"use strict";

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
   phone: {type: String, required:true},
   addedAt: {type: Date, default: Date.now}
}, {
    versionKey: false,
    collection: 'UserCollection'
});

module.exports = mongoose.model('UsersModel',UserSchema);