import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
    key: 'my-app',
    storage: localStorage
})

export default new Vuex.Store({
  state: {
    user: {
        isLoggedIn: false
    },
    bottomNav: 'home'
  },
  mutations: {
    login(state) {
        state.user.isLoggedIn = true;
    },
    logout(state) {
        state.user.isLoggedIn = false;
    },
    changeBottomNav(state, data) {
        state.bottomNav = data;
    }
  },
  actions: {

  },
  plugins: [vuexPersist.plugin]
})
