import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router);

export default new Router({
  mode: process.env.CORDOVA_PLATFORM ? 'hash' : 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/reservations',
      name: 'reservations',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Reservations.vue')
    },
      {
          path: '/search',
          name: 'search',
          component: () => import('./views/Search.vue')
      },
      {
          path: '/masters',
          name: 'masters',
          component: () => import('./views/Masters.vue')
      },
      {
          path: '/chat',
          name: 'chat',
          component: () => import('./views/Chat.vue')
      },


  ]
})
